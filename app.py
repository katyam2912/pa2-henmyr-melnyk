from tkinter import *
from PIL import Image, ImageTk
import melnyk_henmyr as mh


mh.main()

root = Tk()
root.title("Gaming statistics and sales")
root.geometry("540x300")

# Inserting the logo
logo = Image.open("logo.jpeg").resize((526, 130), Image.ANTIALIAS)
logo = ImageTk.PhotoImage(logo)
logo_label = Label(root,image=logo)
logo_label.image = logo
logo_label.pack()

instructions = Label(root, text="Select one of the following options: ", font=('Raleway', 18, 'bold'))
instructions.pack()

## Queries section

# Sales-related queries
# 1. View
query_1 = "SELECT * FROM games_revenue"

# View: Revenue for a single game
query_2 = "SELECT Game, Revenue " \
    "FROM games_revenue " \
    "WHERE Game = \"{}\""

# View: Revenue for a specific genre or type of genre
query_3 = "SELECT Genre, SUM(revenue) AS 'Revenue' " \
    "FROM games_revenue " \
    "WHERE Genre LIKE '%{}%' " \
    "GROUP BY Genre " \
    "ORDER BY Revenue DESC "

# View: Total sales revenue for a specific developer
query_4 = "SELECT Developer, SUM(Revenue) AS 'Revenue' " \
    "FROM games_revenue " \
    "WHERE Developer = \"{}\""

# Total sales revenue for a specific publisher
query_5 = "SELECT Publisher, SUM(Revenue) AS 'Revenue' " \
    "FROM games_revenue " \
    "WHERE Publisher = \"{}\""

# View: Genres - revenue
query_6 = "SELECT Genre, SUM(revenue) AS 'Revenue' " \
    "FROM games_revenue " \
    "GROUP BY Genre " \
    "ORDER BY Revenue DESC " \

# View: Developers - revenue
query_7 = "SELECT Developer, SUM(revenue) AS 'Revenue' " \
    "FROM games_revenue " \
    "GROUP BY Developer " \
    "ORDER BY Revenue DESC "

# View: Publishers - revenue
query_8 = "SELECT Publisher, SUM(revenue) AS 'Revenue' " \
    "FROM games_revenue " \
    "GROUP BY Publisher " \
    "ORDER BY Revenue DESC " \

# Player-related queries
# 2. User playtime distributed across games
query_9 = "SELECT user_name AS 'Player' FROM players"
query_10 = "SELECT title AS 'Game', SUM(time_played) AS 'Hours played' " \
    "FROM games JOIN players_games ON players_games.game_id = games.game_id " \
    "JOIN players ON players.player_id = players_games.player_id " \
    "WHERE user_name = \"{}\" " \
    "GROUP BY title " \
    "ORDER BY SUM(time_played) DESC"

# 3. User playtime distributed across genres
# query_9 is used to list all players
query_11 = "SELECT genre AS 'Genre', SUM(time_played) AS 'Hours played' " \
    "FROM games JOIN players_games ON players_games.game_id = games.game_id " \
    "JOIN players ON players.player_id = players_games.player_id " \
    "WHERE user_name = \"{}\" " \
    "GROUP BY genre " \
    "ORDER BY SUM(time_played) DESC"

# 4. Total hours played for a specific game by all players
# query_12 is used to give to user a variety of games from database
query_12 = "SELECT Game FROM games_revenue ORDER BY Game ASC"
query_13 = "SELECT title AS 'Game', SUM(time_played) AS 'Total hours played' " \
    "FROM games JOIN players_games ON games.game_id = players_games.game_id " \
    "WHERE title = \"{}\""

# 5. Total hours played for a specific genre by all players
query_14 = "SELECT genre FROM games GROUP BY genre ORDER BY genre ASC"
query_15 = "SELECT genre AS 'Genre', SUM(time_played) as 'Hours played' " \
    "FROM games JOIN players_games ON players_games.game_id = games.game_id " \
    "WHERE genre = \"{}\""

# Date-related queries
# 6. Sales for a given game the first week + preorders
# query_12 is used to give to user a variety of games from database
query_16 = "SELECT title AS 'Game', COUNT(players_games.game_id) AS 'Sales' " \
    "FROM games JOIN players_games ON games.game_id = players_games.game_id " \
    "WHERE DATEDIFF(purchase_date, release_date_world) < 7 AND title = \"{}\""

# 7. Post-patch purchases for a specific game
# query_7 is used to give to user a variety of games from database
query_17 = "SELECT title AS 'Game', COUNT(player_id) AS 'Post-patch purchases' " \
    "FROM games JOIN players_games ON games.game_id = players_games.game_id " \
    "JOIN games_versions ON games.game_id = games_versions.game_id " \
    "WHERE DATEDIFF(purchase_date, release_date) > 0 AND title = \"{}\""

# 8. Top 10 most preordered games
# just direct query
query_18 = "SELECT title AS 'Game', COUNT(players_games.game_id) AS 'Preorders' " \
    "FROM games JOIN players_games ON games.game_id = players_games.game_id " \
    "WHERE DATEDIFF(purchase_date, release_date_world) < 0 " \
    "GROUP BY title " \
    "ORDER BY Preorders DESC " \
    "LIMIT 10"
  
# 9. Top 10 most sold games since a given date
query_19 = "SELECT release_date_world AS 'Earliest release date' FROM games ORDER BY release_date_world ASC LIMIT 1"
query_20 = "SELECT title AS 'Game', COUNT(players_games.game_id) AS 'Sales' " \
    "FROM games JOIN players_games ON games.game_id = players_games.game_id " \
    "WHERE DATEDIFF(purchase_date, \"{}\") > 0 " \
    "GROUP BY title " \
    "ORDER BY Sales DESC " \
    "LIMIT 10"


# Functions declaration

# create text box widget with return of the query  
def create_textBox(popWindow,width_int, height_int,query):
  txt_box = Text(popWindow, width=width_int, height=height_int)
  txt_box.pack()
  return_query=mh.onclick(query)
  txt_box.insert(END, return_query)

# retrieve the content of the text box widget (serves as the input box)
def retrieve_input(txt, secondquery, popWindow):
    inputValue=txt.get()
    create_textBox(popWindow,60, 5,secondquery.format(inputValue))

# does as it sounds, creates buttons with binded retrieve function.
# buttons names are stored in dict....idk why, maybe we don't need this, but
# it's like a sequence of available button names
def button_create(string,txt, query, popWindow):
  buttons = {1:"Commit", 2:"Single game", 3:"Single genre (LIKE)", 4:"Single developer", 5:"Single publisher", 6:"Genres - revenue", 7:"Developers - revenue", 8:"Publishers - revenue"}
  if string in buttons.values():
    btn = Button(popWindow,width=15, height=1, text=f"{string}", command=lambda: 
    retrieve_input(txt, query, popWindow)).pack()


# what should be done when user switches betwen menu options
def outputIntoTextBox(firstquery, secondquery):
  popWindow = Toplevel()
  popWindow.attributes("-topmost", True)
  if firstquery is None:
    create_textBox(popWindow, 60, 15, secondquery)
  elif firstquery is not None:
    create_textBox(popWindow, 50,15, firstquery)
    input_label = Label(popWindow, text = "Please insert input below", bg = "white")
    input_label.pack()
    txt=Entry(popWindow, width=50)
    txt.pack()
    button_create("Commit",txt, secondquery, popWindow)
  else:
    print("Something went wrong")


# what should be done if user chooses View (1 menu option)
def outputView(query_1, query_2, query_3, query_4, query_5, query_6, query_7, query_8):
  popWindow = Toplevel()
  popWindow.attributes("-topmost", True)
  create_textBox(popWindow,170, 10,query_1)
  input_label = Label(popWindow, text = "Please insert input below", bg = "white")
  input_label.pack()
  txt=Entry(popWindow,width=50)
  txt.pack()
  button_create("Single game",txt, query_2, popWindow)
  button_create("Single genre (LIKE)",txt, query_3, popWindow)
  button_create("Single developer",txt, query_4, popWindow)
  button_create("Single publisher",txt, query_5, popWindow)
  button_create("Genres - revenue",txt, query_6, popWindow)
  button_create("Developers - revenue",txt, query_7, popWindow)
  button_create("Publishers - revenue",txt, query_8, popWindow)
  
# Switch between menu options 
def execute(*args):   
  if clicked.get() == options[1]:
    outputView(query_1, query_2, query_3, query_4, query_5, query_6, query_7, query_8)
  elif clicked.get() == options[2]:
    outputIntoTextBox(query_9, query_10)
  elif clicked.get() == options[3]:
    outputIntoTextBox(query_9, query_11)
  elif clicked.get() == options[4]:
    outputIntoTextBox(query_12, query_13)
  elif clicked.get() == options[5]:
    outputIntoTextBox(query_14, query_15)
  elif clicked.get() == options[6]:
    outputIntoTextBox(query_12, query_16)
  elif clicked.get() == options[7]:
    outputIntoTextBox(query_12, query_17) 
  elif clicked.get() == options[8]:
    outputIntoTextBox(None, query_18)
  elif clicked.get() == options[9]:
    outputIntoTextBox(query_19, query_20)
  

# Variables and function call
options = ["Click to select a query", 
           "1. View: games_revenue", 
           "2. User playtime distributed across games",
           "3. User playtime distributed across genres",
           "4. Total hours played for a specific game by all players",
           "5. Total hours played for a specific genre by all players",
           "6. Sales for a given game the first week + preorders",
           "7. Post-patch purchases for a specific game",
           "8. Top 10 most preordered games",
           "9. Top 10 most sold games since a given date"
           ]
clicked = StringVar(root)
clicked.set(options[0])
clicked.trace("w",execute)
dropMenu = OptionMenu(root, clicked, *options)
dropMenu.pack()


root.mainloop()
mh.cnx.close()
