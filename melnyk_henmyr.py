import mysql.connector
import csv
from mysql.connector import errorcode
from tabulate import tabulate


cnx = mysql.connector.connect(user='root',
                              password='root',
                              host='localhost',
                              port='3306'
                              # unix_socket= '/Applications/MAMP/tmp/mysql/mysql.sock',
                              )

DB_NAME = 'melnyk_henmyr'  # Kateryna Melnyk, Albert Henmyr
# .csv files should be in the same directory as the .py file in order for the code to run

# Checks if database exists, creates it if doesn't

def check_database(cursor, DB_NAME):
    try:
        cursor.execute(f'USE {DB_NAME}')
        return True
    except mysql.connector.Error as err:
        print(f'Database {DB_NAME} does not exist.')
        if err.errno == errorcode.ER_BAD_DB_ERROR:
            create_database(cursor, DB_NAME)
            print(f'Database {DB_NAME} created successfully.')
            cnx.database = DB_NAME
            return False
        else:
            print(err)

# Called by check_database if no database called DB_NAME exists


def create_database(cursor, DB_NAME):
    try:
        cursor.execute(
            f"CREATE DATABASE {DB_NAME} DEFAULT CHARACTER SET 'utf8'")
    except mysql.connector.Error as err:
        print(f"Failed creating database: {err}")
        exit(1)

# This function can be filled with data to generate more tables according to the existing pattern of:
# 1) Table creation string
# 2) Data insertion string
# 3) File name string
# The for loop at the end will send each triplet of strings in the right direction so long as they are inserted in the right order.
# Called by main after check_database if database had to be created


def generate_tables(cursor):

    vardata = []

    # Variables for players
    vardata.append("CREATE TABLE `players` ("
                "  `player_id` int(200) NOT NULL,"
                "  `user_name` varchar(50) NOT NULL,"
                "  `country` varchar(50) DEFAULT NULL,"
                "  `birth_date` date DEFAULT NULL,"
                "  `gender` varchar(20) DEFAULT NULL,"
                "   PRIMARY KEY (`player_id`)"
                ") ENGINE=InnoDB")
    vardata.append("INSERT INTO players VALUES (%(player_id)s,%(user_name)s,%(country)s, \
    %(birth_date)s,%(gender)s );")
    vardata.append('players.csv')

    vardata.append("CREATE TABLE `games` ("
                "  `game_id` int(200) NOT NULL,"
                "  `title` varchar(200) NOT NULL,"
                "  `genre` varchar(200) DEFAULT NULL,"
                "  `developer` varchar(200) NOT NULL,"
                "  `publisher` varchar(200) NOT NULL,"
                "  `release_date_JP` date DEFAULT NULL,"
                "  `release_date_world` date DEFAULT NULL,"
                "  `price_kr` int(200) NOT NULL,"
                "   PRIMARY KEY (`game_id`)"
                ") ENGINE=InnoDB")
    vardata.append("INSERT INTO games VALUES (%(game_id)s,%(title)s,%(genre)s, \
    %(developer)s,%(publisher)s, %(release_date_JP)s,%(release_date_world)s,%(price_kr)s );")
    vardata.append('games.csv')

    vardata.append("CREATE TABLE `players_games` ("
                "  `player_id` int(200) NOT NULL,"
                "  `game_id` int(200) NOT NULL,"
                "  `purchase_date` date NOT NULL,"
                "  `time_played` int(200) NOT NULL,"
                "   PRIMARY KEY (`player_id`,`game_id`),"
                "   FOREIGN KEY (`player_id`) REFERENCES players(`player_id`),"
                "   FOREIGN KEY (`game_id`) REFERENCES games(`game_id`)"
                ") ENGINE=InnoDB")
    vardata.append("INSERT INTO players_games VALUES (%(player_id)s,%(game_id)s,%(purchase_date)s, \
    %(time_played)s );")
    vardata.append('players_games.csv')

    vardata.append("CREATE TABLE `games_versions` ("
                "  `patch_id` int(200) NOT NULL,"
                "  `game_id` int(200) NOT NULL,"
                "  `release_date` date NOT NULL,"
                "  `type` varchar(200) NOT NULL,"
                "  `patch_notes` varchar(200) NOT NULL,"
                "   PRIMARY KEY (`patch_id`),"
                " FOREIGN KEY (`game_id`) REFERENCES games(`game_id`)"
                ") ENGINE=InnoDB")
    vardata.append("INSERT INTO games_versions VALUES (%(patch_id)s,%(game_id)s,%(release_date)s,%(type)s, \
    %(patch_notes)s );")
    vardata.append('games_versions.csv')

    for i in range(len(vardata) // 3):
        create_tables(cursor, vardata[3 * i])
        insert_data(cursor, vardata[3 * i + 1], vardata[3 * i + 2])

# Called by generate_tables
def create_tables(cursor, string):
    try:
        print("Creating table: ")
        cursor.execute(string)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)
    else:
        print("OK")

# Populates the tables
# Called by generate_tables


def insert_data(cursor, string, path):
    print(f"Inserting data from {path}...")
    with open(path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            dict_obj = {}
            for key, value in row.items():
                dict_obj[key] = value
                if value == 'Unreleased':
                    dict_obj[key] = None
            try:
                cursor.execute(string, dict_obj)
            except mysql.connector.Error as err:
                print(err.msg)
            else:
                cnx.commit()
    print("OK")

def create_view(cursor):
    cursor.execute("CREATE VIEW `games_revenue` AS " \
    "SELECT title AS 'Game', genre AS 'Genre', developer AS 'Developer', publisher AS 'Publisher', " \
    "price_kr * COUNT(player_id) AS 'Revenue' " \
    "FROM games JOIN players_games ON games.game_id = players_games.game_id " \
    "GROUP BY title " \
    "ORDER BY Revenue DESC")



# Unites all functions for creation of database
def main():
    cursor = cnx.cursor()
    # Connect to DB and ensure it exists
    dbexists = check_database(cursor, DB_NAME)
    if not dbexists:
        generate_tables(cursor)
        create_view(cursor)
    cursor.close()

# Called for returning the result of the query and displaying it with tabulate
def onclick(query):
    cursor = cnx.cursor()
    # Run the actual query
    cursor.execute(query)
    result = tabulate(cursor, headers = "keys")
    return result
    cursor.close()





