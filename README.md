# Installation details:

1. Please pull repository from https://gitlab.com/katyam2912/pa2-henmyr-melnyk
After that, all necessary files should be located in one directory.

2. The password to the database connection in melnyk_henmyr.py is changed to "root".
user='root', password='root'

3. Please be sure that you have the following modules/packages installed before running any of python files:

    - pip install tabulate

    - tkinter should be a part of standard library, so no installation is required

    - pip install Pillow

4. Please open your terminal and change current working directory  to pa2-henmyr-melnyk
5. After that use command :  `python3 app.py`
  - first of all, melnyk_henmyr database will be created
  - after that GUI window will appear
